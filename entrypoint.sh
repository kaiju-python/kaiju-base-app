#!/usr/bin/env sh

set -e
python3 -Om app "$@"
