from kaiju_tools.interfaces import PublicInterface
from kaiju_tools.services import ContextableService
from kaiju_tools.rpc import debug_only

__all__ = ['EchoService']


class EchoService(ContextableService, PublicInterface):
    """Simple echo."""

    service_name = 'echo'

    def __init__(self, *args, **kws):
        super().__init__(*args, **kws)

    @property
    def routes(self) -> dict:
        """Public methods."""
        return {'echo': self.echo, 'echo_session': self.echo_session}

    @property
    def validators(self) -> dict:
        """Public methods input params validators"""
        return {}

    @property
    def permissions(self) -> dict:
        return {'*': self.PermissionKeys.GLOBAL_GUEST_PERMISSION}

    @debug_only
    async def echo(self, data: dict = None) -> dict:
        """Echo back the arguments and session."""
        return data

    @debug_only
    async def echo_session(self):
        return self.get_session()
