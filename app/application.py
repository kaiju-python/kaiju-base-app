"""Application instance initialization."""

from pathlib import Path

import aiohttp_jinja2
import jinja2

from kaiju_tools import App
from kaiju_tools.app import init_app as init_app_base
from kaiju_tools.http import JSONRPCView

from app.tables import METADATA
from app.views import *

__all__ = ['init_app', 'URLS']

URLS = [('*', '/public/rpc', JSONRPCView, 'json_rpc_view')]  #: web app routes


def init_app(settings, **kws) -> App:
    _dir = Path(__file__).parent
    app = init_app_base(settings, attrs={'db_meta': METADATA}, **kws)
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(_dir / 'static' / 'bundle', followlinks=True))
    app.router.add_static('/static/', path=str(_dir.parent / 'static'))
    for method, reg, handler, name in URLS:
        app.router.add_route(method, reg, handler)
        app.router.add_route(method, reg + '/', handler, name=name)
    return app
