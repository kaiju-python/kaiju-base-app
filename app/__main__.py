"""Server runner.

Use `python -m app` to start the server or `python -m app --help` for a list of commands.
"""

from kaiju_tools.app import run_server
from app.application import init_app

if __name__ == '__main__':
    run_server(
        init_app,
        base_config_paths=[
            './settings/config.yml',
            './settings/services/common.yml',
            './settings/services/app.yml',
            './settings/services/tasks.yml',
        ],
        base_env_paths=['./settings/env.base.json'],
        default_env_paths=['./settings/env.local.json'],
    )
