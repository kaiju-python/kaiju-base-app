import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

from kaiju_db.functions import UserFunction, SQL_FUNCTIONS

__all__ = ['METADATA']

METADATA = sa.MetaData()  #: user tables metadata
