"""Package initialization.

Do not remove any imports even the 'unused' ones.
"""

import asyncio
from warnings import warn

import kaiju_tools.services
import kaiju_auth.services
import kaiju_tasks.services
import kaiju_db.services
import kaiju_redis.services
from kaiju_tools.app import COMMANDS, SERVICE_CLASS_REGISTRY

import app.commands
from app import services
from app.application import init_app
from app.tables import *

try:
    import uvloop

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ModuleNotFoundError:
    warn('uvloop is not installed; use `pip install uvloop` for better performance')

SERVICE_CLASS_REGISTRY.register_from_module(services)
COMMANDS.register_from_module(app.commands)

__author__ = 'antonnidhoggr@me.com'
__email__ = 'antonnidhoggr@me.com'
__version__ = '1.0.0'
