import pytest
import pytest_asyncio

from app.application import init_app

from kaiju_tools.app import ConfigLoader
from kaiju_tools.http import HTTPService, RPCClientService
from kaiju_tools.encoding import load

from kaiju_tools.tests.fixtures import get_app

__all__ = ['web_app', 'web_client', 'test_user', 'get_client']


PORT = 9999


@pytest.fixture(scope='session')
def test_user():
    with open('./fixtures/users/users.json') as f:
        data = load(f)
        return next(row['data'] for row in data if row['data']['username'] == 'pytest')


@pytest_asyncio.fixture
async def web_app(aiohttp_server):
    config_loader = ConfigLoader(
        base_config_paths=[
            './settings/config.yml',
            './settings/services/common.yml',
            './settings/services/app.yml',
            './settings/services/tasks.yml',
        ],
        base_env_paths=['./settings/env.base.json', './settings/env.pytest.json'],
        default_env_paths=['./settings/env.pytest.local.json'],
    )
    command, settings = config_loader.configure()
    _app = init_app(settings)
    server = await aiohttp_server(_app, port=PORT, access_log=False)  # TODO: random port ?
    async with server:
        yield _app


def get_client(app):
    transport = HTTPService(app=app, host=f'http://0.0.0.0:{PORT}', request_logs=False, response_logs=False)
    client = RPCClientService(app=app, transport=transport)
    app.services.add_service(client)
    return client


@pytest_asyncio.fixture
async def web_client(app):
    client = get_client(app)
    async with app.services:
        yield client
