import asyncio
import logging
from concurrent.futures import ProcessPoolExecutor
from time import time

import pytest

from kaiju_tools.encoding import dumps, load
from kaiju_tools.tests.fixtures import get_app

from tests.fixtures import get_client


with open('./etc/req.json') as f:
    DATA = load(f)
    DATA = dumps(DATA)


@pytest.mark.benchmark
@pytest.mark.asyncio
class TestPerformance:
    """Test common application methods."""

    requests = 10000
    parallel = 128
    _counter = 0
    _data = DATA

    async def _run_client(self):
        _client = get_client(get_app(logging.getLogger('client')))
        _client._request_logs = False
        _client._response_logs = False
        async with _client.app.services:
            while 1:
                await _client._transport.request('post', '/public/rpc', data=self._data, headers={}, accept_json=False)
                self._counter += 1
                if self._counter > self.requests:
                    return

    async def test_performance(self, web_app, logger):
        web_app.services.rpc._enable_permissions = False
        # web_app.services.rpc._request_logs = False
        # web_app.services.rpc._response_logs = False
        t0 = time()
        await asyncio.gather(*(self._run_client() for _ in range(self.parallel)))
        total = time() - t0
        rpc = round(self._counter / total)
        logger.info(f'{self._counter} total; {rpc}/s')
