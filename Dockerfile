FROM python:3.11 as app-base

ENV PORT 9999
ENV PYTHONOPTIMIZE 1
WORKDIR /app
ENTRYPOINT ["./entrypoint.sh"]
EXPOSE ${PORT}
STOPSIGNAL SIGINT
COPY requirements.docker ./
RUN apt-get update && \
    apt-get install -y $(cat requirements.docker | grep -v '#' | xargs) && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/apt/lists/*

FROM app-base AS app-req

COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

FROM app-req as app-final

COPY . /app
