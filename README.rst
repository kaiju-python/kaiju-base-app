Summary
-------

**Python** >=3.8
**Postgres** >=14
**Redis** >=7.0

Run
---

Assuming you have already configured database and redis environment:

.. code-block:: console

  python -m venv venv
  ./venv/bin/activate
  pip install -U setuptools pip wheel
  pip install -r requirements.txt
  python -m app -f settings/env.local.json

For more run options:

.. code-block::

  python -m app --help

Tests
-----

You should write your tests inside `tests/` package. Use standard `web_app` and `web_client` fixtures to get access
to the public methods.

How to run tests:

.. code-block:: console

  pip install -r requirements.dev.txt
  docker-compose -f docker-compose.yaml up -d postgres redis
  pytest

Development
-----------

How to setup the app for development (example with pyenv).

.. code-block:: console

  pyenv local 3.8.16
  python -m venv venv
  tools/init.sh

How to run the app in dev mode locally.

.. code-block:: console

  docker-compose -f docker-compose.yaml up -d postgres redis
  python -m app -f settings/env.development.json -f settings/env.local.json

Same but with INFO log:

.. code-block::

  python -m app -f settings/env.development.json -f settings/env.local.json --log=INFO

Same but with task manager and executor:

.. code-block:: console

  python -m app -f settings/env.development.json -f settings/env.manager.json -f settings/env.executor.json -f settings/env.local.json
